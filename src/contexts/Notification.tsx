import { createContext, FC, useCallback, useContext, useState } from 'react';
import { NotificationListItemProps } from '../components/NotificationList/components/Item/useNotificationListItem';
import { StyledNotificationList, NotificationListItem } from '../components/NotificationList';
import { v4 as uuidv4 } from 'uuid';

type NotificationType = 'success' | 'error';

export type NotificationContextProps = {
    showNotification: (message: string, type: NotificationType) => void;
};

export const NotificationContext = createContext<NotificationContextProps>({
    showNotification: () => undefined
});

export const NotificationProvider: FC = ({ children }) => {
    const [notifications, setNotifications] = useState<NotificationListItemProps[]>([]);

    const showNotification: NotificationContextProps['showNotification'] = useCallback((message, type = 'success') => {
        setNotifications((oldNotifications) => [...oldNotifications, { id: uuidv4(), message, type }]);
    }, []);

    return (
        <NotificationContext.Provider value={{ showNotification }}>
            {children}
            <StyledNotificationList spacing={2}>
                {notifications.map(({ id, message, type }) => (
                    <NotificationListItem
                        key={id}
                        id={id}
                        message={message}
                        type={type}
                        autoHideTimeout={4000}
                        onClose={() =>
                            setNotifications((oldNotifications) =>
                                oldNotifications.filter((notification) => notification.id !== id)
                            )
                        }
                    />
                ))}
            </StyledNotificationList>
        </NotificationContext.Provider>
    );
};

export const useNotificationContext = () => useContext(NotificationContext);
