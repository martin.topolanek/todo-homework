import { createContext, FC, useCallback, useContext, useEffect, useState } from 'react';
import { TodoApi, TodoTaskRequestProps, TodoTaskResponseProps } from '../api/Todo';
import { useNotificationContext } from './Notification';

type TodoContextProps = {
    isInitialLoading: boolean;
    updatingTaskIds: Set<TodoTaskResponseProps['id']>;
    tasks: Map<TodoTaskResponseProps['id'], TodoTaskResponseProps>;
    getTaskById: (taskId: TodoTaskResponseProps['id'] | undefined) => TodoTaskResponseProps | undefined;
    updateTask: (taskId: TodoTaskResponseProps['id'], task: Partial<TodoTaskRequestProps>) => void;
    addTask: (task: Omit<TodoTaskRequestProps, 'completed'> & Partial<Pick<TodoTaskRequestProps, 'completed'>>) => void;
    deleteTask: (taskId: TodoTaskResponseProps['id']) => void;
};

export const TodoContext = createContext<TodoContextProps>({
    isInitialLoading: true,
    updatingTaskIds: new Set(),
    tasks: new Map(),
    getTaskById: () => undefined,
    updateTask: () => undefined,
    addTask: () => undefined,
    deleteTask: () => undefined
});

export const TodoProvider: FC = ({ children }) => {
    const todoApi = new TodoApi();
    const { showNotification } = useNotificationContext();
    const [taskItems, setTaskItems] = useState<Map<TodoTaskResponseProps['id'], TodoTaskResponseProps>>(new Map());
    const [updatingTaskIds, setUpdatingTaskIds] = useState<Set<TodoTaskResponseProps['id']>>(new Set());
    const [isInitialLoading, setInitialLoading] = useState<boolean>(true);

    const getTaskById: TodoContextProps['getTaskById'] = useCallback(
        (taskId) => {
            return !isInitialLoading && typeof taskId === 'number' ? taskItems.get(taskId) : undefined;
        },
        [taskItems, isInitialLoading]
    );

    const updateTask: TodoContextProps['updateTask'] = useCallback(async (taskId, task) => {
        const update = async () => {
            try {
                const response = await todoApi
                    .updateTask(taskId, task)
                    .finally(() =>
                        setUpdatingTaskIds((oldTaskIds) => new Set([...oldTaskIds].filter((id) => id !== taskId)))
                    );

                if (response.status === 200 && response.data.success) {
                    const updatedTask = response.data.data;
                    setTaskItems((oldTaskItems) => new Map(oldTaskItems.set(taskId, updatedTask)));
                    showNotification(`Task '${updatedTask?.title}' was updated successful.`, 'success');
                } else {
                    showNotification(`Updating of task ${getTaskById(taskId)?.title} failed.`, 'error');
                }
            } catch (error) {
                showNotification(`${error}`, 'error');
            }
        };

        setUpdatingTaskIds((oldTaskIds) => new Set(oldTaskIds.add(taskId)));
        update();
    }, []);

    const addTask: TodoContextProps['addTask'] = useCallback(
        ({ title, description, completed = false }) => {
            const add = async () => {
                try {
                    if (title) {
                        const response = await todoApi.addTask({ title, description, completed });

                        if (response.status === 200 && response.data.success) {
                            const addedTask = response.data.data;
                            setTaskItems((oldTaskItems) => new Map(oldTaskItems.set(addedTask.id, addedTask)));
                            showNotification(`Task ${addedTask.title} was added successfuly.`, 'success');
                        } else {
                            showNotification(`Task was not added. Something went wrong. Please try again.`, 'error');
                        }
                    } else {
                        showNotification(`Title must be provided.`, 'error');
                    }
                } catch (error) {
                    showNotification(`${error}`, 'error');
                }
            };

            add();
        },
        [setTaskItems, showNotification]
    );

    const deleteTask: TodoContextProps['deleteTask'] = useCallback(
        async (taskId) => {
            try {
                const response = await todoApi.deleteTask(taskId);

                if (response.status === 200 && response.data.success) {
                    setTaskItems(
                        (oldTaskItems) =>
                            new Map(
                                [...oldTaskItems]
                                    .filter(([_, task]) => task.id !== taskId)
                                    .map(([key, task]) => [key, task])
                            )
                    );
                    showNotification(`Task was deleted successfuly.`, 'success');
                } else {
                    showNotification(`Task was not deleted. Something went wrong. Please try again.`, 'error');
                }
            } catch (error) {
                showNotification(`${error}`, 'error');
            }
        },
        [setTaskItems, showNotification]
    );

    const fetchApi = useCallback(async () => {
        try {
            const response = await todoApi.getAllTasks();

            if (response.status === 200) {
                setTaskItems(
                    new Map<TodoTaskResponseProps['id'], TodoTaskResponseProps>(
                        response.data.data.map((task) => [task.id, task])
                    )
                );
            }
        } catch (error) {
            showNotification(`${error}`, 'error');
        } finally {
            setInitialLoading(false);
        }
    }, [setInitialLoading, setTaskItems]);

    useEffect(() => {
        fetchApi();
    }, [fetchApi]);

    return (
        <TodoContext.Provider
            value={{
                isInitialLoading,
                updatingTaskIds,
                tasks: taskItems,
                getTaskById,
                updateTask,
                addTask,
                deleteTask
            }}
        >
            {children}
        </TodoContext.Provider>
    );
};

export const useTodoContext = () => useContext<TodoContextProps>(TodoContext);
