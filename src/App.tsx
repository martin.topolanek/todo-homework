import React from 'react';
import { createTheme, CssBaseline, ThemeProvider } from '@mui/material';
import { OverviewPage } from './pages/Overview';
import { TaskDetailPage } from './pages/TaskDetail';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { TodoProvider } from './contexts/Todo';
import { NotificationProvider } from './contexts/Notification';

function App() {
    const theme = createTheme();

    return (
        <ThemeProvider theme={theme}>
            <NotificationProvider>
                <TodoProvider>
                    <CssBaseline />
                    <BrowserRouter>
                        <Routes>
                            <Route path={'/'} element={<OverviewPage />} />
                            <Route path={':taskId'} element={<TaskDetailPage />} />
                        </Routes>
                    </BrowserRouter>
                </TodoProvider>
            </NotificationProvider>
        </ThemeProvider>
    );
}

export default App;
