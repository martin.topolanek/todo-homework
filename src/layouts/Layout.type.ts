export type LayoutMenuItemName = 'overview';
export type LayoutMenuItemsType = Array<{ name: string; isActive?: boolean }>;
