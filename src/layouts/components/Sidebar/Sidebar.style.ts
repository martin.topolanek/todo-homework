import { styled } from '@mui/material/styles';
import MuiDrawer from '@mui/material/Drawer';

export const StyledLayoutSidebar = styled(MuiDrawer)`
    & .MuiDrawer-paper {
        box-sizing: border-box;
        position: relative;
        white-space: nowrap;
        width: var(--drawer-width);
        transition: ${({ theme }) =>
            theme.transitions.create('width', {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.enteringScreen
            })};
    }

    &[data-is-open='false'] > .MuiDrawer-paper {
        overflow-x: hidden;
        transition: ${({ theme }) =>
            theme.transitions.create('width', {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.leavingScreen
            })};
        width: ${({ theme }) => theme.spacing(7)};
    }
`;
