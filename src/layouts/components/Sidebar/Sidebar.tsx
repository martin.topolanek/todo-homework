import { Divider, IconButton, List, ListItemButton, ListItemIcon, ListItemText, Toolbar } from '@mui/material';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import DashboardIcon from '@mui/icons-material/Dashboard';
import { StyledLayoutSidebar } from './Sidebar.style';
import { FC } from 'react';
import { LayoutMenuItemsType } from '../../Layout.type';
import { MuiToRouterButton } from '../../../components/Button';

type LayoutSidebarProps = {
    isDrawerOpen: boolean;
    toggleDrawer: () => void;
    layoutMenuItems: LayoutMenuItemsType;
};

export const LayoutSidebar: FC<LayoutSidebarProps> = ({ isDrawerOpen, toggleDrawer, layoutMenuItems }) => (
    <StyledLayoutSidebar variant={'permanent'} data-is-open={isDrawerOpen}>
        <Toolbar
            sx={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'flex-end',
                px: [1]
            }}
        >
            <IconButton onClick={toggleDrawer}>
                <ChevronLeftIcon />
            </IconButton>
        </Toolbar>
        <Divider />
        <List component="nav">
            {layoutMenuItems.map((menu) => (
                <ListItemButton
                    selected={menu.isActive}
                    key={menu.name}
                    component={MuiToRouterButton as any}
                    href={'/'}
                >
                    <ListItemIcon>
                        <DashboardIcon />
                    </ListItemIcon>
                    <ListItemText primary={menu.name} />
                </ListItemButton>
            ))}
        </List>
    </StyledLayoutSidebar>
);
