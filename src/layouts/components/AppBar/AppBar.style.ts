import { styled } from '@mui/material/styles';
import MuiAppBar from '@mui/material/AppBar';

export const StyledLayoutAppBar = styled(MuiAppBar)`
    z-index: ${({ theme }) => theme.zIndex.drawer + 1};
    transition: ${({ theme }) =>
        theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        })};

    &[data-is-open='true'] {
        margin-left: var(--drawer-width);
        width: calc(100% - var(--drawer-width));
        ${({ theme }) =>
            theme.transitions.create(['width', 'margin'], {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.enteringScreen
            })};
    }
`;
