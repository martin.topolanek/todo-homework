import { IconButton, Toolbar, Typography } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import { StyledLayoutAppBar } from './AppBar.style';
import { FC } from 'react';

type LayoutAppBarProps = {
    isDrawerOpen: boolean;
    toggleDrawer: () => void;
    pageName: string;
};

export const LayoutAppBar: FC<LayoutAppBarProps> = ({ isDrawerOpen, toggleDrawer, pageName }) => (
    <StyledLayoutAppBar position={'absolute'} data-is-open={isDrawerOpen}>
        <Toolbar
            sx={{
                pr: '24px'
            }}
        >
            <IconButton
                edge="start"
                color="inherit"
                aria-label="open drawer"
                onClick={toggleDrawer}
                sx={{
                    marginRight: '36px',
                    ...(isDrawerOpen && { display: 'none' })
                }}
            >
                <MenuIcon />
            </IconButton>
            <Typography component="h1" variant="h6" color="inherit" noWrap sx={{ flexGrow: 1 }}>
                {pageName}
            </Typography>
        </Toolbar>
    </StyledLayoutAppBar>
);
