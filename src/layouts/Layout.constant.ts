import { LayoutMenuItemName } from './Layout.type';

export const LAYOUT_MENU_ITEMS: Record<LayoutMenuItemName, string> = {
    overview: 'Overview'
};
