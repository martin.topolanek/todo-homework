import { styled } from '@mui/material/styles';
import { Box } from '@mui/material';

export const StyledLayoutDefaultContent = styled(Box)`
    overflow: auto;
    padding-top: var(--app-bar-height);
    flex-grow: 1;
    height: 100vh;
    background-color: ${({ theme }) =>
        theme.palette.mode === 'light' ? theme.palette.grey[100] : theme.palette.grey[900]};
`;

export const StyledLayoutDefaultContainer = styled('div')`
    display: flex;
`;
