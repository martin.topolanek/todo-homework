import { useState } from 'react';
import {LayoutMenuItemName, LayoutMenuItemsType} from './Layout.type';
import { LAYOUT_MENU_ITEMS } from './Layout.constant';

type UseLayoutArgs = {
    activeMenu?: LayoutMenuItemName;
    title?: string;
};

export const useLayout = ({ activeMenu, title }: UseLayoutArgs) => {
    const [isDrawerOpen, setDrawerOpen] = useState<boolean>(false);
    const toggleDrawer = () => {
        setDrawerOpen((prevState) => !prevState);
    };
    const layoutMenuItems: LayoutMenuItemsType = Object.entries(LAYOUT_MENU_ITEMS).map(
        ([index, menuName]) => ({
            name: menuName,
            isActive: activeMenu === index
        })
    );
    const pageName = title ? title : activeMenu ? LAYOUT_MENU_ITEMS[activeMenu] : 'Dashboard';

    return {
        isDrawerOpen,
        toggleDrawer,
        layoutMenuItems,
        pageName
    };
};
