import { wrap } from '../utils/wrap';
import { Container } from '@mui/material';
import { useLayout } from './useLayout';
import { LayoutAppBar, LayoutSidebar } from './components';
import { StyledLayoutDefaultContainer, StyledLayoutDefaultContent } from './Layout.style';

export const Layout = wrap(
    ({ isDrawerOpen, toggleDrawer, layoutMenuItems, pageName, children }) => (
        <StyledLayoutDefaultContainer>
            <LayoutAppBar isDrawerOpen={isDrawerOpen} toggleDrawer={toggleDrawer} pageName={pageName} />
            <LayoutSidebar isDrawerOpen={isDrawerOpen} toggleDrawer={toggleDrawer} layoutMenuItems={layoutMenuItems} />
            <StyledLayoutDefaultContent component="main">
                <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
                    {children}
                </Container>
            </StyledLayoutDefaultContent>
        </StyledLayoutDefaultContainer>
    ),
    useLayout
);
