import { wrap } from '../../utils/wrap';
import { Paper } from '@mui/material';
import { Layout } from '../../layouts';
import { Task } from '../../components/Task';
import { useTaskDetail } from './useTaskDetail';

export const TaskDetailPage = wrap(
    ({ task, isInitialLoading }) => (
        <Layout title={`Task: ${task?.title}`}>
            <Paper sx={{ p: 2 }}>
                <Task
                    name={task?.title}
                    completed={task?.completed}
                    description={task?.description}
                    isLoading={isInitialLoading}
                />
            </Paper>
        </Layout>
    ),
    useTaskDetail
);
