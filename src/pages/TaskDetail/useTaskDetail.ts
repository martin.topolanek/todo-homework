import { useParams } from 'react-router-dom';
import { useTodoContext } from '../../contexts/Todo';
import { TodoTaskRequestProps } from '../../api/Todo';
import { useMemo } from 'react';

export const useTaskDetail = () => {
    const params = useParams();
    const taskId = params.taskId ? parseInt(params.taskId, 10) : undefined;
    const { getTaskById, isInitialLoading } = useTodoContext();
    const task: TodoTaskRequestProps | undefined = typeof taskId === 'number' ? useMemo(() => getTaskById(taskId), [taskId, getTaskById]) : undefined;

    return { task, isInitialLoading };
};
