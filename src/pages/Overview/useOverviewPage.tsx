import { useTodoContext } from '../../contexts/Todo';
import { useCallback, useState } from 'react';
import { TodoTaskResponseProps } from '../../api/Todo';

export const useOverviewPage = () => {
    const { tasks, isInitialLoading } = useTodoContext();
    const [isCreateDialogOpen, setCreateDialogOpen] = useState<boolean>(false);
    const tableRows: TodoTaskResponseProps[] = [];

    const toggleCreateTaskModal = useCallback(() => {
        setCreateDialogOpen((prevState) => !prevState);
    }, [setCreateDialogOpen]);

    for (const task of tasks.values()) {
        tableRows.push(task);
    }

    return {
        tableRows,
        isInitialLoading,
        isCreateDialogOpen,
        toggleCreateTaskModal
    };
};
