import { Button, Box, Grid, Paper, Typography } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import { wrap } from '../../utils/wrap';
import { Layout } from '../../layouts';
import { useOverviewPage } from './useOverviewPage';
import { TasksTable } from '../../components/TasksTable';
import { DialogCreateTask } from '../../components/Dialog';
import { NotFound } from '../../components/NotFound';

export const OverviewPage = wrap(
    ({ isInitialLoading, toggleCreateTaskModal, isCreateDialogOpen, tableRows }) => (
        <Layout activeMenu={'overview'}>
            <Paper>
                <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', p: 2 }}>
                    <Typography variant={'h4'} component={'h2'}>
                        {`Task list`}
                    </Typography>

                    <Button
                        variant={'contained'}
                        color={'success'}
                        onClick={toggleCreateTaskModal}
                        startIcon={<AddIcon />}
                    >
                        {`Add task`}
                    </Button>
                    <DialogCreateTask isOpen={isCreateDialogOpen} closeModal={toggleCreateTaskModal} />
                </Box>
                {(tableRows.length > 0 || isInitialLoading) ? (
                    <Grid container>
                        <Grid item xs={12}>
                            <TasksTable rows={tableRows} isLoading={isInitialLoading} />
                        </Grid>
                    </Grid>
                ) : (
                    <NotFound />
                )}
            </Paper>
        </Layout>
    ),
    useOverviewPage
);
