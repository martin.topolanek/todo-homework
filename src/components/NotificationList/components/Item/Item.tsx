import { wrap } from '../../../../utils/wrap';
import { useNotificationListItem } from './useNotificationListItem';
import { Alert } from '@mui/material';

export const NotificationListItem = wrap(
    ({ message, type, onClose }) => (
        <Alert variant={'filled'} severity={type} onClose={() => onClose?.()}>
            {message}
        </Alert>
    ),
    useNotificationListItem
);
