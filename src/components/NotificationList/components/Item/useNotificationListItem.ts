import { useEffect } from 'react';

export type NotificationListItemProps = {
    id: string;
    autoHideTimeout?: number;
    onClose?: () => void;
    message: string;
    type?: 'success' | 'error';
};

export const useNotificationListItem = ({
    autoHideTimeout = 2000,
    type = 'success',
    onClose,
    ...args
}: NotificationListItemProps) => {
    useEffect(() => {
        setTimeout(() => {
            onClose?.();
        }, autoHideTimeout);
    }, []);

    return {
        type,
        onClose,
        ...args
    };
};
