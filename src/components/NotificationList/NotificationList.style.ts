import { styled } from '@mui/material/styles';
import { Stack } from '@mui/material';

export const StyledNotificationList = styled(Stack)`
    position: fixed;
    top: 20px;
    right: 20px;
    z-index: 10000;
`;
