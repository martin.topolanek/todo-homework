import { wrap } from '../../../../utils/wrap';
import { useTasksTableRow } from './useTasksTableRow';
import { TableCell, TableRow } from '@mui/material';

export const TasksTableRow = wrap(
    ({ name, completed, button }) => (
        <TableRow>
            <TableCell>{name}</TableCell>
            <TableCell align={'center'}>{completed}</TableCell>
            <TableCell align={'right'}>{button}</TableCell>
        </TableRow>
    ),
    useTasksTableRow
);
