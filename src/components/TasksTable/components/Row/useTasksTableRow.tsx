import { useCallback, useState } from 'react';
import EditIcon from '@mui/icons-material/Edit';
import SearchIcon from '@mui/icons-material/Search';
import DeleteIcon from '@mui/icons-material/Delete';
import { TodoTaskResponseProps } from '../../../../api/Todo';
import { Box, Button, Checkbox } from '@mui/material';
import { DialogUpdateTask } from '../../../Dialog';
import { Button as RouterButton } from '../../../Button';
import { useTodoContext } from '../../../../contexts/Todo';

export type TasksTableRowProps = TodoTaskResponseProps;

export const useTasksTableRow = (task: TasksTableRowProps) => {
    const { updatingTaskIds, updateTask, deleteTask } = useTodoContext();
    const [isEditDialogOpen, setEditDialogOpen] = useState<boolean>(false);

    const toggleEditTaskModal = useCallback(() => {
        setEditDialogOpen((prevState) => !prevState);
    }, [setEditDialogOpen]);

    const row = {
        id: task.id,
        name: task.title,
        completed: (
            <Checkbox
                disabled={updatingTaskIds.has(task.id)}
                checked={task.completed}
                onChange={(event) => {
                    updateTask(task.id, { completed: event.currentTarget.checked });
                }}
            />
        ),
        button: (
            <Box sx={{ display: 'flex', alignItems: 'center', columnGap: (theme) => theme.spacing(1) }}>
                <Button
                    variant={'contained'}
                    color={'warning'}
                    onClick={toggleEditTaskModal}
                    startIcon={<EditIcon />}
                >{`Edit`}</Button>
                <DialogUpdateTask taskId={task.id} closeModal={toggleEditTaskModal} isOpen={isEditDialogOpen} />
                <RouterButton
                    variant={'contained'}
                    href={`/${task.id}`}
                    startIcon={<SearchIcon />}
                >{`Detail`}</RouterButton>
                <Button variant={'text'} color={'error'} size={'small'} onClick={() => deleteTask(task.id)}>
                    <DeleteIcon />
                </Button>
            </Box>
        )
    };

    return { ...row };
};
