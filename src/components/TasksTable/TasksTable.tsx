import { FC } from 'react';
import { TasksTableProps } from './TasksTable.type';
import { Skeleton, Table as MuiTable, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material';
import { TasksTableRow } from './components/Row/Row';

export const TasksTable: FC<TasksTableProps> = ({ rows, isLoading }) => (
    <TableContainer>
        <MuiTable>
            <TableHead>
                <TableRow>
                    <TableCell sx={{ width: '100%' }}>{`Name`}</TableCell>
                    <TableCell align={'center'}>{`Completed`}</TableCell>
                    <TableCell></TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {isLoading &&
                    [...Array(5)].map((_, index) => (
                        <TableRow key={index}>
                            <TableCell>
                                <Skeleton animation={'wave'} variant={'text'} width={`100%`} />
                            </TableCell>
                            <TableCell align={'center'} sx={{ fontSize: 0 }}>
                                <Skeleton
                                    animation={'wave'}
                                    variant={'rectangular'}
                                    width={`20px`}
                                    height={`20px`}
                                    sx={{ display: 'inline-block' }}
                                />
                            </TableCell>
                            <TableCell align={'center'}>
                                <Skeleton animation={'wave'} variant={'rectangular'} width={`80px`} height={`35px`} />
                            </TableCell>
                        </TableRow>
                    ))}
                {rows.map((row) => (
                    <TasksTableRow {...row} key={row.id} />
                ))}
            </TableBody>
        </MuiTable>
    </TableContainer>
);
