import { TodoTaskResponseProps } from '../../api/Todo';

export type TasksTableProps = {
    isLoading?: boolean;
    rows: TodoTaskResponseProps[];
};
