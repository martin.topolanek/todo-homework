import { FC, Fragment } from 'react';
import { StyledTaskContainer, StyledTaskDescription, StyledTaskName, StyledTaskState } from './Task.style';
import { Skeleton } from '@mui/material';

type TaskProps = {
    name?: string;
    description?: string;
    completed?: boolean;
    isLoading?: boolean;
};

export const Task: FC<TaskProps> = ({ name, description, completed, isLoading }) => (
    <StyledTaskContainer>
        <StyledTaskName variant={'h6'} component={'h1'}>
            {isLoading ? <Skeleton animation={'wave'} variant={'text'} width={'350px'} /> : name}
        </StyledTaskName>
        <StyledTaskState variant={'body2'}>
            {isLoading ? (
                <Skeleton animation={'wave'} variant={'text'} width={`80px`} />
            ) : (
                <Fragment>{completed ? 'Completed' : 'Not completed'}</Fragment>
            )}
        </StyledTaskState>
        {(description || isLoading) && (
            <StyledTaskDescription variant={'body2'}>
                {isLoading ? (
                    <>
                        <Skeleton animation={'wave'} variant={'text'} width={`100%`} />
                        <Skeleton animation={'wave'} variant={'text'} width={`60%`} />
                    </>
                ) : (
                    description
                )}
            </StyledTaskDescription>
        )}
    </StyledTaskContainer>
);
