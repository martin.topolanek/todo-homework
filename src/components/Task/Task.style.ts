import { styled } from '@mui/material/styles';
import { Box, Typography } from '@mui/material';

export const StyledTaskName = Typography;

export const StyledTaskDescription = styled(Typography)`
    width: 100%;
    color: ${({theme}) => theme.palette.grey[600]}
`;

export const StyledTaskState = styled(Typography)`
    color: ${({ theme }) => theme.palette.grey[600]};
    margin: 0;
`;

export const StyledTaskContainer = styled(Box)`
    display: flex;
    justify-content: space-between;
    align-items: center;
    flex-wrap: wrap;
    row-gap: ${({theme}) => theme.spacing(1)};
    padding: ${({ theme }) => theme.spacing(1, 2)};
`;
