import { wrap } from '../../utils/wrap';
import { DialogTaskView } from './views/Task';
import { useDialogUpdateTask } from './hooks/useDialogUpdateTask';

export const DialogUpdateTask = wrap(DialogTaskView, useDialogUpdateTask);
