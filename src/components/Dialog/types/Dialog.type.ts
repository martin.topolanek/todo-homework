export type DialogProps = {
    isOpen: boolean;
    closeModal: () => void;
};
