import { wrap } from '../../utils/wrap';
import { DialogTaskView } from './views/Task';
import { useDialogCreateTask } from './hooks/useDialogCreateTask';

export const DialogCreateTask = wrap(DialogTaskView, useDialogCreateTask);
