import { DialogTaskViewProps } from '../views/Task';
import { DialogProps } from '../types/Dialog.type';
import { useCallback, useRef } from 'react';
import { useTodoContext } from '../../../contexts/Todo';

type UseDialogCreateTaskArgs = DialogProps;

export const useDialogCreateTask = ({ closeModal, ...args }: UseDialogCreateTaskArgs): DialogTaskViewProps => {
    const { addTask } = useTodoContext();
    const titleInputRef = useRef<HTMLInputElement | null>(null);
    const descriptionInputRef = useRef<HTMLInputElement | null>(null);
    const onConfirm = useCallback(() => {
        addTask({
            title: titleInputRef.current?.querySelector('input')?.value ?? '',
            description: descriptionInputRef.current?.querySelector('textarea')?.value ?? ''
        });

        console.log(descriptionInputRef.current?.querySelector('textarea')?.value);

        closeModal();
    }, [addTask, titleInputRef, descriptionInputRef, closeModal]);

    return {
        ...args,
        title: 'Add task',
        titleInput: {
            ref: titleInputRef
        },
        descriptionInput: {
            ref: descriptionInputRef
        },
        onConfirm,
        closeModal
    };
};
