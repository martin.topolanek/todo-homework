import { DialogTaskViewProps } from '../views/Task';
import { DialogProps } from '../types/Dialog.type';
import { useCallback, useRef } from 'react';
import { TodoTaskResponseProps } from '../../../api/Todo';
import { useTodoContext } from '../../../contexts/Todo';

type UseDialogUpdateTaskArgs = {
    taskId: TodoTaskResponseProps['id'];
} & DialogProps;

export const useDialogUpdateTask = ({ taskId, closeModal, ...args }: UseDialogUpdateTaskArgs): DialogTaskViewProps => {
    const { getTaskById, updateTask } = useTodoContext();
    const titleInputRef = useRef<HTMLInputElement | null>(null);
    const descriptionInputRef = useRef<HTMLInputElement | null>(null);
    const updatedTask = getTaskById(taskId);
    const onConfirm = useCallback(() => {
        updateTask(taskId, {
            title: titleInputRef.current?.querySelector('input')?.value ?? '',
            description: descriptionInputRef.current?.querySelector('textarea')?.value ?? ''
        });

        closeModal();
    }, [updateTask, titleInputRef, descriptionInputRef, closeModal]);

    return {
        ...args,
        title: `Edit task`,
        titleInput: {
            ref: titleInputRef,
            defaultValue: updatedTask?.title
        },
        descriptionInput: {
            ref: descriptionInputRef,
            defaultValue: updatedTask?.description
        },
        onConfirm,
        closeModal
    };
};
