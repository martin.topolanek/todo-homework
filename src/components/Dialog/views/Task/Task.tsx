import { VFC } from 'react';
import { Button, InputProps, TextField } from '@mui/material';
import { DialogProps } from '../../types/Dialog.type';
import {
    StyledDialogTaskActions,
    StyledDialogTaskContainer,
    StyledDialogTaskContent,
    StyledDialogTaskTitle
} from './Task.style';

export type DialogTaskViewProps = {
    title: string;
    titleInput: InputProps;
    descriptionInput: InputProps;
    onConfirm: () => void;
} & DialogProps;

export const DialogTaskView: VFC<DialogTaskViewProps> = ({
    isOpen,
    closeModal,
    title,
    titleInput,
    descriptionInput,
    onConfirm
}) => (
    <StyledDialogTaskContainer open={isOpen} onClose={closeModal}>
        <StyledDialogTaskTitle>{title}</StyledDialogTaskTitle>
        <StyledDialogTaskContent>
            <TextField size={'small'} fullWidth label={'Title'} InputProps={titleInput} />
            <TextField
                size={'small'}
                multiline
                rows={5}
                fullWidth
                label={'Description'}
                InputProps={descriptionInput}
            />
        </StyledDialogTaskContent>
        <StyledDialogTaskActions>
            <Button onClick={onConfirm}>Confirm</Button>
        </StyledDialogTaskActions>
    </StyledDialogTaskContainer>
);
