import { styled } from '@mui/material/styles';
import { Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';

export const StyledDialogTaskTitle = DialogTitle;

export const StyledDialogTaskContent = styled(DialogContent)`
    overflow: visible;
    display: flex;
    flex-direction: column;
    row-gap: ${({ theme }) => theme.spacing(1)};
`;

export const StyledDialogTaskActions = DialogActions;

export const StyledDialogTaskContainer = styled(Dialog)`
    .MuiDialog-paper {
        width: 100%;
    }
`;
