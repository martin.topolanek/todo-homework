import { forwardRef } from 'react';
import { Link as RouterLink, LinkProps as RouterLinkProps } from 'react-router-dom';
import { default as MuiButton, ButtonProps as MuiButtonProps } from '@mui/material/Button';

export const MuiToRouterButton = forwardRef<any, Omit<RouterLinkProps, 'to'> & { href: RouterLinkProps['to'] }>(
    ({ href, ...props }, ref) => <RouterLink ref={ref} to={href} {...props} />
);

export const Button = forwardRef<any, MuiButtonProps>((props, ref) => (
    <MuiButton component={MuiToRouterButton as any} {...props} ref={ref} />
));
