import { styled } from '@mui/material/styles';
import { Box } from '@mui/material';

export const StyledNotFoundContainer = styled(Box)`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    row-gap: ${({ theme }) => theme.spacing(3)};
    height: 400px;

    svg {
        width: 2em;
        height: 2em;
    }

    p {
        font-size: 28px;
        color: ${({theme}) => theme.palette.grey[700]};
    }
`;
