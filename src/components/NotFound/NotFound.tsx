import { FC } from 'react';
import ListAltIcon from '@mui/icons-material/ListAlt';
import { StyledNotFoundContainer } from './NotFound.style';
import { Typography } from '@mui/material';

type NotFoundProps = {};

export const NotFound: FC<NotFoundProps> = () => (
    <StyledNotFoundContainer>
        <ListAltIcon fontSize={'large'} />
        <Typography>{`Your todo list is empty.`}</Typography>
    </StyledNotFoundContainer>
);
