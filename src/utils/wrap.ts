import { createElement, FC } from 'react';

export const wrap =
    <ComponentProps extends {} = {}, HookArgs extends {} = {}>(
        Component: FC<ComponentProps>,
        useCustomHook: (args: HookArgs) => ComponentProps
    ): FC<HookArgs> =>
    (props) =>
        createElement(Component, useCustomHook(props), props.children);
