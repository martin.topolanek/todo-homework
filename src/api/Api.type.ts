import { AxiosRequestConfig, AxiosResponse } from 'axios';

export type ApiRequestConfig = AxiosRequestConfig;

export type ApiResponse<T extends {} = {}> = AxiosResponse<T & { success: boolean }>;
