import { ApiRequestConfig } from './Api.type';

export const API_COMMON_CONFIG: ApiRequestConfig = {
    headers: {
        'Content-Type': 'application/json'
    }
};
