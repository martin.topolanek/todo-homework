import { ApiRequestConfig } from '../Api.type';
import { API_COMMON_CONFIG } from '../Api.constant';
import axios  from 'axios';
import {
    TodoAddTaskResponse,
    TodoDeleteTaskResponse,
    TodoGetAllTaskResponse,
    TodoTaskRequestProps,
    TodoTaskResponseProps,
    TodoUpdateTaskResponse
} from './Todo.type';
import { UserApi } from '../User';

const TODO_API_CONFIG: ApiRequestConfig = {
    ...API_COMMON_CONFIG,
    url: 'https://todo-homework.herokuapp.com',
    headers: {
        Authorization: 'Bearer'
    }
};

export class TodoApi {
    protected userApi;

    constructor() {
        this.userApi = new UserApi();
    }

    protected getAuthConfig = async (): Promise<ApiRequestConfig> => {
        const token = await this.userApi.getToken();

        return {
            ...TODO_API_CONFIG,
            headers: {
                ...TODO_API_CONFIG.headers,
                Authorization: `Bearer ${token}`
            }
        };
    };

    getAllTasks = async (): Promise<TodoGetAllTaskResponse> => {
        const config = await this.getAuthConfig();

        return axios.request({ ...config, method: 'GET', url: `${config.url}/getAllTasks` });
    };

    deleteTask = async (taskId: TodoTaskResponseProps['id']): Promise<TodoDeleteTaskResponse> => {
        const config = await this.getAuthConfig();

        return axios.request({ ...config, method: 'POST', url: `${config.url}/deleteTask/${taskId}` });
    };

    addTask = async (task: TodoTaskRequestProps): Promise<TodoAddTaskResponse> => {
        const config = await this.getAuthConfig();

        return axios.request({
            ...config,
            method: 'POST',
            url: `${config.url}/addTask`,
            data: task
        });
    };

    updateTask = async (
        taskId: TodoTaskResponseProps['id'],
        task: Partial<TodoTaskRequestProps>
    ): Promise<TodoUpdateTaskResponse> => {
        const config = await this.getAuthConfig();

        return axios.request({
            ...config,
            method: 'POST',
            url: `${config.url}/updateTask/${taskId}`,
            data: task
        });
    };
}
