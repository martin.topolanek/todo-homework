import { ApiResponse } from '../Api.type';

export type TodoTaskRequestProps = {
    title: string;
    description?: string;
    completed: boolean;
};

export type TodoTaskResponseProps = {
    id: number;
} & TodoTaskRequestProps;

export type TodoGetAllTaskResponse = ApiResponse<{ data: TodoTaskResponseProps[] }>;
export type TodoAddTaskResponse = ApiResponse<{ data: TodoTaskResponseProps }>;
export type TodoUpdateTaskResponse = ApiResponse<{ data: TodoTaskResponseProps }>;
export type TodoDeleteTaskResponse = ApiResponse;
