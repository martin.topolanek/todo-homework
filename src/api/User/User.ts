import axios from 'axios';
import { API_COMMON_CONFIG } from '../Api.constant';
import { UserRegisterResponse, UserRequestProps } from './User.type';
import faker from '@faker-js/faker';

export class UserApi {
    protected register = async (user: UserRequestProps): Promise<UserRegisterResponse> => {
        return axios.request({
            ...API_COMMON_CONFIG,
            url: 'https://todo-homework.herokuapp.com/sign',
            method: 'POST',
            data: user
        });
    };

    getToken = async (): Promise<string> => {
        if (!window.localStorage.getItem('token')) {
            const user: UserRequestProps = {
                name: `${faker.name.firstName()} ${faker.name.lastName()}`,
                password: faker.internet.password(),
                email: faker.internet.email()
            };

            const registerResponse = await this.register(user);

            if (registerResponse?.status === 200) {
                window.localStorage.setItem('token', registerResponse.data.accessToken);
            }
        }

        return window.localStorage.getItem('token') ?? '';
    };
}
