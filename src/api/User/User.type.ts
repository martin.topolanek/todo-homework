import { ApiResponse } from '../Api.type';

export type UserRequestProps = {
    name: string;
    email: string;
    password: string;
};

export type UserRegisterResponse = ApiResponse<{ accessToken: string }>;
